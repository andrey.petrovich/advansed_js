class Employee {
    constructor(name, age, salary) {
      this._name = name;
      this._age = age;
      this._salary = salary;
    }
    set name(val) {
      this._name = val;
    }
    get name() {
      return this._name;
    }
    set age(val) {
      this._age = val;
    }
    get age() {
      return this._age;
    }
    set salary(val) {
      this._salary = val;
    }
    get salary() {
      return this._salary;
    }
  }
  
  class Programmer extends Employee {
    constructor(name, age, salary, lang) {
      super(name, age, salary);
      this._lang = lang;
    }
    get salary() {
      return this._salary * 3;
    }
    set lang(val) {
      this._lang = val;
    }
    get lang() {
      return this._lang;
    }
  }
  
  const prog1 = new Programmer("Dima", 25, 1000, ["eng", "ua", "ru"]);
  const prog2 = new Programmer("Sasha", 30, 1500, ["eng", "ua"]);
  const prog3 = new Programmer("Vika", 35, 700, ["eng", "ru"]);
  const prog4 = new Programmer("Olga", 40, 1200, ["eng", "ua", "pol"]);
  
  console.log(prog1);
  console.log(prog2);
  console.log(prog3);
  console.log(prog4);
  
  